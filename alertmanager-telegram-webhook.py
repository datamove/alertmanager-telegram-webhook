#!/usr/bin/python3

"""
The Alertmanager will send HTTP POST requests in the following JSON format to the configured endpoint:

{
  "version": "4",
  "groupKey": <string>,    // key identifying the group of alerts (e.g. to deduplicate)
  "status": "<resolved|firing>",
  "receiver": <string>,
  "groupLabels": <object>,
  "commonLabels": <object>,
  "commonAnnotations": <object>,
  "externalURL": <string>,  // backlink to the Alertmanager.
  "alerts": [
    {
      "status": "<resolved|firing>",
      "labels": <object>,
      "annotations": <object>,
      "startsAt": "<rfc3339>",
      "endsAt": "<rfc3339>",
      "generatorURL": <string> // identifies the entity that caused the alert
    },
    ...
  ]
}
"""

"""
Example command for testing
curl -XPOST --data '{"status":"resolved","groupLabels":{"alertname":"instance_down"},"commonAnnotations":{"dl90bac100 of job ec2-sp-node_exporter has been down for more than 2 minutes.","summary":"Instance i-0d7188fkl90bac100 down"},"alerts":[{"status":"resolved","labels":{"name":"olokinho01-prod","instance":"i-0d7188fkl90bac100","job":"ec2-sp-node_exporter","alertname":"instance_down","os":"linux","severity":"page"},"endsAt":"2019-07-01T16:16:19.376244942-03:00","generatorURL":"http://pmts.io:9090","startsAt":"2019-07-01T16:02:19.376245319-03:00","annotations":{"description":"i-0d7188fkl90bac100 of job ec2-sp-node_exporter has been down for more than 2 minutes.","summary":"Instance i-0d7188fkl90bac100 down"}}],"version":"4","receiver":"infra-alert","externalURL":"http://alm.io:9093","commonLabels":{"name":"olokinho01-prod","instance":"i-0d7188fkl90bac100","job":"ec2-sp-node_exporter","alertname":"instance_down","os":"linux","severity":"page"}}' http://10.0.1.208:8044/alert
"""

"""
Example sending message:
    curl -X POST https://api.telegram.org/bot${TOKEN}/sendMessage --data '{"chat_id": "${CHAT_ID}", "text": "status_resolved"}' -H 'Content-Type: application/json'
"""

"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
Credit: https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7
"""
from http.client import HTTPSConnection
from http.server import BaseHTTPRequestHandler, HTTPServer

import logging
import json
import os

class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path), str(self.headers), post_data.decode('utf-8'))
        prom_alert = json.loads(post_data)
        alerts_status = prom_alert["status"]
        alerts = prom_alert["alerts"]

        token = os.environ["TOKEN"]
        chat_id = os.environ["CHAT_ID"]
         
        for alert in alerts:
            alert_annotations = alert["annotations"]
            summary = alert_annotations["summary"]
            description = alert_annotations["description"]
            message = "ALERT {}\n{}\n{}".format(alerts_status, summary, description)
            logging.info(message)
            tg_sendMessage(token, chat_id, message)

        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def tg_sendMessage(token, chat_id, message):
  url = 'api.telegram.org'
  conn = HTTPSConnection(url)
  headers = {'Content-type': 'application/json'}

  data = {'chat_id': chat_id, 'text': message}
  json_data = json.dumps(data)
  handle = "/bot{}/sendMessage".format(token)
  conn.request('POST', handle, json_data, headers)

  response = conn.getresponse()
  logging.info(response.read().decode())

def run(server_class=HTTPServer, handler_class=S, port=None):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run(port=8044)


